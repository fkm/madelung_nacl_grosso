#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Robert P. Grosso Jr., Justin T. Fermann, and William J. Vining
# "An In-Depth Look at the Madelung Constant for Cubic Crystal Systems"
#  https://doi.org/10.1021/ed078p1198 

import numpy as np
import math
from itertools import product
import matplotlib.pyplot as plt

def mad_sum(n):
    
    # Liste aus MeshGrid
    x, y, z = np.meshgrid(np.arange(1,n+1), np.arange(n+1), np.arange(n+1), indexing='ij')

    # Maskieren um Werte herauszufiltern: x >= y >= z
    mask = (x >= y) & (y >= z)

    # Kombiniere x, y, z unter Benutzung der Maske
    result = np.column_stack([x[mask], y[mask], z[mask]])
    # Am Ende: 1 0 0, 1 1 0, 1 1 1, 2 0 0, 2 1 0, 2 1 1, 2 2 0, 2 2 1, 2 2 2, 3 0 0, ....

    # Berechnung der Zahl der einzigartigen Werte; z.B, 0 1 2 => 2
    # Berechnung der Zahl der Were, die nicht Null sind; z.B. 0 0 3 => 1
    uniq_val = np.array([len(np.unique(row)) for row in result])
    nonz_val = np.count_nonzero(result, axis=1)

    # Berechnung der Multiplizitaet (>>Multiplizitaet<</sqrt(x**2 + y**2 +z**2) + ...
    calculate_mult = np.vectorize(lambda u, n: 6 / math.factorial(4 - u) * 2**n)
    mult_list = calculate_mult(uniq_val, nonz_val)
    
    # Madelung Komponenten berechnen, Ergebnis aus Multiplizitaet<</sqrt(x**2 + y**2 +z**2)
    mad = (-1)**(result[:,0]+result[:,1]+result[:,2]) * \
          mult_list/np.sqrt(result[:,0]**2+result[:,1]**2+result[:,2]**2)
    
    # Slicing bis zur vorletzen Komponente
    # "inx_last_val" ist Zahl der Indices der letzten Komponenten; 
    # z.B. n = 5: alle [5 0 0] bis [5 5 5] 
    inx_last_val_all = int(n * (n+1) / 2 + n + 1)
    all_sec_last = mad[0:-inx_last_val_all]
    
    # Slicing letzte Komponente
    # Zahl der Indices der letzten Komponenten; z.B. n = 5: alle [5 0 0] bis [5 5 5] 
    # Slicing Flaechen, Kanten, Ecken
    # Multiplikation Flaechen*1/2, Kanten*1/4 und Ecken*1/8 (siehe Paper)
    
    # Zahl aller Komponenten
    n_comp = result.shape[0]
    # Indices bis zur letzen Komponente
    inx_to_last_val_all = int(n_comp - inx_last_val_all)     
    # Zahl der Atome auf Flaechen der letzen Komponente
    n_face = int(n*(n+1)/2)
    # Zahl der Atome auf Kanten der letzen Komponente 
    n_edge = int(n)                                           
    
    # Atome auf Flaechen [n_last y z] (n_last = letzter Index n)
    face_last = mad[inx_to_last_val_all:(inx_to_last_val_all + n_face)]*1/2
    # Atome auf Kanten [n_last _nlast z] (n_last = letzter Index n)
    edge_last = mad[(inx_to_last_val_all+n_face):(inx_to_last_val_all+n_face+n_edge)]*1/4
    # Atome auf Ecken [n_last _nlast n_last] (n_last = letzter Index n)
    corner_last = mad[-1]*1/8
    
    madelung = np.sum(all_sec_last)+np.sum(face_last)+np.sum(edge_last)+np.sum(corner_last)
    
    return madelung

def find_all_solutions_for_sum_of_squares(n):
    solutions = []
    max_val = int(np.sqrt(n)) + 1
    for x, y, z in product(range(-max_val, max_val), repeat=3):
        if x**2 + y**2 + z**2 == n:
            solutions.append((x, y, z))
    return len(solutions)

def madelung_wrong(n):
    madelung = 0
    mpn_w = []
    for i in range(1, n+1):
        numerator = find_all_solutions_for_sum_of_squares(i)
        denominator = np.sqrt(i)
        mpn_w.append(madelung + (-1)**(i-1) * numerator / denominator)
        if numerator:
            #print((-1)**(i-1)*numerator,denominator**2)
            madelung += (-1)**(i-1) * numerator / denominator
    return mpn_w, madelung


N = 40

#Klassische Methode
mpn_w, result = madelung_wrong(N)
#klassisch
print(f'Madelung-Konstante für NaCl mit N={N} (klassisch)    :', '{:.4f}'.format(result))
#nach Grosso et al.
result = mad_sum(N)
print(f'Madelung-Konstante für NaCl mit N={N} (Grosso et al.):', '{:.4f}'.format(result))

# Werte für Plot nach Grosso et al. erzeugen
# langsam und nicht optimiert
mad_sing = [mad_sum(N) for N in range(1, N+1)]

fig = plt.figure()
ax  = fig.add_subplot()
x = np.arange(1, N+1, 1)
ax.scatter(x,mpn_w,label='klassisch')
ax.scatter(x,mad_sing,label='Grosso et al.')
ax.set_xlim(1, N+1)
ax.set_ylim(-12, 12)
ax.set_yticks(np.arange(-12,12,3))
ax.set_xticks(np.arange(0, N+1, int(N/10)))
ax.hlines(-1.747564594633, 0, N+1, color='red',label = '-1.7476',linestyles='dashed')
ax.set_title('Berechnung der Madelung-Konstante für NaCl')
ax.set_ylabel('Madelung-Konstante')
ax.set_xlabel('Anzahl der Sphären')
ax.legend()
plt.tight_layout()
plt.show()